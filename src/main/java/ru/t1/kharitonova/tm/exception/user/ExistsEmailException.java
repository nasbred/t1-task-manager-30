package ru.t1.kharitonova.tm.exception.user;

import ru.t1.kharitonova.tm.exception.AbstractException;

public class ExistsEmailException extends AbstractException {

    public ExistsEmailException() {
        super("Error! Email already exists.");
    }

}
