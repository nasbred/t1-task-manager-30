package ru.t1.kharitonova.tm.exception.user;

import ru.t1.kharitonova.tm.exception.AbstractException;

public class EmailEmptyException extends AbstractException {

    public EmailEmptyException() {
        super("Error! Email is empty.");
    }

}
