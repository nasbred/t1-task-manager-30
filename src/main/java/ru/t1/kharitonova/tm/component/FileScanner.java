package ru.t1.kharitonova.tm.component;

import lombok.SneakyThrows;
import ru.t1.kharitonova.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class FileScanner extends Thread {

    private Bootstrap bootstrap;

    private final List<String> commands = new ArrayList<>();

    private final File folder = new File("./");

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            process();
        }
    }

    public FileScanner(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    public void init() {
        final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandsWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        start();
    }

    private void process() {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    bootstrap.runCommand(fileName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
